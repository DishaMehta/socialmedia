const fs = require('fs');
const method = require('./AfterLogin');
class PostService {

    static addPost(post) {
        fs.readFile('./Posts.json', (err, rows) => {
            if (err) {
                console.log(err);
            }
            else {

                let data = JSON.parse(rows);
                for (let temp of data) {
                    if (temp.email == post.email) {
                        console.log("alreadty present")
                        return;
                    }
                }
                data.push(post)
                let Data = JSON.stringify(data);
                fs.writeFile('./Posts.json', Data, (err, res) => {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        console.log("data succesfully enteered")
                    }
                })
            }
        })

    }

    static viewPostByFriends(userEmail) {
        fs.readFile('./User.json', (err, rows1) => {
            if (err) {
                console.log(err);
            }
            else {
                let data = JSON.parse(rows1);
                data.filter(item => item.email == userEmail);
                fs.readFile('./Posts.json', (err, rows) => {
                    if (err) {
                        console.log(err);
                        method.optfun();
                    }
                    else {
                        let posts = JSON.parse(rows);
                        data[0].friends.forEach(element => {
                            for (let temp of posts) {
                                if (temp.postByEmailId == element) {
                                    console.log(temp);
                                }
                            }
                        })
                        method.optfun();
                    }
                })
            }
        })
    }

    static getPost(userEmail) {
            fs.readFile('./Posts.json', (err, rows) => {
                if (err) {
                    console.log(err);
                }
                else {

                    let data = JSON.parse(rows);
                    for (let temp of data) {
                        if (temp.postByEmailId == userEmail) {
                            console.log(temp);
                        }
                        else {
                            console.log("User is not Present");
                            method.optfun();
                            return;
                        }
                    }
                    method.optfun();
                }

            })
        }
    static deletePost(id) {
            fs.readFile('./Posts.json', (err, rows) => {
                if (err) {
                    console.log(err);
                    method.optfun();
                }
                else {
                    const obj = JSON.parse(rows);
                    const data = obj.filter(item => {
                        if (item.id == id && item.postByEmailId == process.env.Email) {
                            return false;
                        }
                        else
                            return true;
                    })
                    fs.writeFileSync('./Posts.json', JSON.stringify(data));
                    method.optfun();
                }
            })


        }
}

    const obj = {
        "id": 4,
        "text": "First post",
        "postByEmailId": "roy@gmail.com",
        "postByName": "Roy",
        "likesByEmailId": ["alice@gmail.com", "moni@gmail.com"]
    }

module.exports = PostService;
