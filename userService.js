const fs = require('fs');
const optfun = require('./AfterLogin');

class UserService {

    static addUser(user) {
        return new Promise( (res,rej)=>{
            fs.readFile('User.json', 'utf8', (err, data)=>{
                if (err){
                    rej({
                        status:{
                            success:false,
                            message:'Error while adding user'
                        }
                    })
                    return;
                } else {
                    var obj = JSON.parse(data); //now it an objec
                    for(var tmp of obj){
                    if(tmp.email == user.email){
                       rej({
                            status:{
                                success:false,
                                message:'User Already exist'
                            }
                       })
                       return; 
                    }
                    }
                    obj.push(user); //add some data
                    let json = JSON.stringify(obj);
                    fs.writeFile('User.json', json, 'utf8', (err)=> {
                        if(err){
                            rej( {
                                status: {
                                    success: false,
                                    message: 'fail'
                                }
                            })
                            return;
                        }
                        else{
                            res( {
                                status: {
                                    success: true,
                                    message: 'success'
                                }
                            })
                        }
                    })
                }
                
            })
        });  
      
}

    static removeUser(userEmail) {
        return new Promise( (res,rej)=>{
            fs.readFile('User.json', 'utf8', (err, data)=>{
                if (err){
                    rej({
                        status:{
                            success:false,
                            message:'Error while reading user'
                        }
                    })
                } else {
                    var obj = JSON.parse(data);
                    var convertedData = obj.filter(item=>item.email != userEmail)
                    fs.writeFile('User.json', JSON.stringify(convertedData), 'utf8', (err)=> {
                        if(err){
                            rej( {
                                status: {
                                    success: false,
                                    message: 'failed to remove'
                                }
                            })
                            return;
                        }
                        else{
                            res( {
                                status: {
                                    success: true,
                                    message: 'successfully removed'
                                }
                            })
                        }
                    })
                }
            })
        });
    }


    static viewUser(userEmail) {
        return new Promise( (res,rej)=>{
            fs.readFile('User.json', 'utf8', (err, data)=>{
                if (err){
                    rej({
                        status:{
                            success:false,
                            message:'Error while viewing user'
                        }
                    })
                } else {
                    var obj = JSON.parse(data);
                    for(var tmp of obj){
                        if(tmp.email == userEmail){
                        res({
                                status:{
                                    success:true,
                                    message:'User Found'
                                },
                                result:{
                                    email: tmp.email,
                                    name: tmp.name,
                                    friends: tmp.friends
                                }
                        }) 
                        }
                    }
                }
            })
        });
    }


    static listUsers(userEmail) {
        return new Promise( (res,rej)=>{
            fs.readFile('User.json', 'utf8', (err, data)=>{
                if (err){
                    rej({
                        status:{
                            success:false,
                            message:'Error while listing user'
                        }
                    })
                } else {
                    var obj = JSON.parse(data);
                        res({
                                status:{
                                    success:true,
                                    message:'Success'
                                },
                                result:
                                    obj.filter(item=> item.email !== userEmail).map(item => {
                                        return {email: item.email, name: item.name};
                                    })
                                
                        }) 
                }
            })
        });
    }
}
var us = {
    email :"aeny@gmail.com",
    name :"Aeny",
    password :"90d27c92d91f6f16a4214bbaab683a6b23f9ca581d21ccea1d59a3ab524613e095221cf9612c41e214a29c0b06c6a34acd1ad2ae6b625697d4510bc5c0959587",
    friends :[]
}

//UserService.addUser(us).then(console.log).catch(console.log);
//UserService.viewUser("roy@gmail.com").then(console.log).catch(console.log);
//UserService.listUsers("roy@gmail.com").then(console.log).catch(console.log);
//UserService.removeUser("aeny@gmail.com").then(console.log).catch(console.log);
// console.log(ans);
module.exports = UserService;
