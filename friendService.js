const fs = require('fs');
const method = require('./AfterLogin');

class FriendService {

    static addFriend(userEmailId, friendEmailId) {

        if (userEmailId != friendEmailId) {
            fs.readFile('User.json', 'utf8', function readFileCallback(err, data) {

                if (err) {
                    console.log(err);
                } else {
                    let obj = JSON.parse(data);

                    for (let user of obj) {
                        if (user.email == userEmailId) {
                            if(!user.friends.includes(friendEmailId)){
                                user.friends.push(friendEmailId);
                            }
                            else{
                                console.log(`${friendEmailId} is already your friend`);
                                method.optfun();
                                return;
                            }
                        }
                    }

                    let json = JSON.stringify(obj);
                    fs.writeFile('User.json', json, 'utf8', (err) => {
                        if (err) {
                            console.error(err);
                            return;
                        };
                        console.log("Friend has been added");
                        method.optfun();
                    })
                }
            });
        }
        else{
            console.log('Users can not add themselves!');
        }
    }

    static removeFriend(userEmailId, friendEmailId) {
        
        fs.readFile('User.json', 'utf8', function readFileCallback(err, data) {

            if (err) {
                console.log(err);
            } else {
                let obj = JSON.parse(data);

                for (let user of obj) {
                    if (user.email == userEmailId) {
                        if(user.friends.includes(friendEmailId)){
                            let index = user.friends.indexOf(friendEmailId);
                            user.friends.splice(index, 1);
                        }
                        else{
                            console.log(`${friendEmailId} is not your friend`);
                            method.optfun();
                            return;
                        }
                    }
                }

                let json = JSON.stringify(obj);
                fs.writeFile('User.json', json, 'utf8', (err) => {
                    if (err) {
                        console.error(err);
                        return;
                    };
                    console.log("Friend has been removed.");
                    method.optfun();
                })
            }
        });
    }

    static viewFriend(userEmailId, friendEmailId) {
        
        fs.readFile('User.json', 'utf8', function readFileCallback(err, data) {

            if (err) {
                console.log(err);
            } else {
                let obj = JSON.parse(data);

                for (let user of obj) {
                    if (user.email == userEmailId) {
                        if(user.friends.includes(friendEmailId)){
                            console.log(obj.filter(item => item.email == friendEmailId).map(item => {
                                return {
                                    email: item.email,
                                    name: item.name,
                                    friends: item.friends
                                };
                            }));
                            method.optfun();
                        }
                        else{
                            console.log(`${friendEmailId} is not your friend`);
                            method.optfun();
                            return;
                        }
                    }
                }
            }
        });
    }

    static viewFriends(userEmailId) {
        fs.readFile('User.json', 'utf8', function readFileCallback(err, data) {

            if (err) {
                console.log(err);
            } else {
                let obj = JSON.parse(data);

                for (let user of obj) {
                    if (user.email == userEmailId) {
                        user.friends.forEach(element => {
                            console.log(obj.filter(item => item.email == element).map(item => {
                                return {
                                    email: item.email,
                                    name: item.name,
                                    friends: item.friends
                                };
                            }));
                        });
                        method.optfun();
                    }
                }
            }
        });
        
    }
}


module.exports = FriendService;
